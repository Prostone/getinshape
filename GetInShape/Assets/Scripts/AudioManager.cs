using UnityEngine.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

    public AudioMixerGroup musicMixerGroup;
    public AudioMixerGroup soundMixerGroup;

    public float mainDesiredPitch = 1;
    public float pitchChangeSpeed = 0.01f;

    public Slider musicSlider;
    public Slider soundSlider;
    private string musicKey = "MusicVolume";
    private string soundKey = "SoundVolume";
    private float musicVolumeSetting;
    private float soundVolumeSetting;
    private float musicDefault = -20f;
    private float soundDefault = -20f;

    public Sound[] sounds;
    private Sound main;
    private Sound click;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start ()
	{
        GetOptionsFromPlayerPrefs();
        SetSliderValues();

        foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.loop = s.loop;

            s.source.outputAudioMixerGroup = s.mixerGroup;
		}

        main = Array.Find(sounds, item => item.name == "MainTheme");
        click = Array.Find(sounds, item => item.name == "TimerClick");

        Play("MainTheme");

    }

    void SetSliderValues()
    {
        musicSlider.value = musicVolumeSetting;
        soundSlider.value = soundVolumeSetting;
    }

    void SetVolumes()
    {
        soundMixerGroup.audioMixer.SetFloat("SoundVolume", soundVolumeSetting);
        musicMixerGroup.audioMixer.SetFloat("MusicVolume", musicVolumeSetting);
    }

    void GetOptionsFromPlayerPrefs()
    {
        musicVolumeSetting = PlayerPrefs.GetFloat(musicKey, musicDefault);
        soundVolumeSetting = PlayerPrefs.GetFloat(soundKey, soundDefault);
        SetVolumes();
    }

    public void ChangeMusicVolumeBasedOnSlider()
    {
        musicVolumeSetting = musicSlider.value;
        PlayerPrefs.SetFloat(musicKey, musicVolumeSetting);
        SetVolumes();
    }

    public void ChangeSoundVolumeBasedOnSlider()
    {
        soundVolumeSetting = soundSlider.value;
        PlayerPrefs.SetFloat(soundKey, soundVolumeSetting);
        SetVolumes();
    }

    public void PlayTimerClick(float timeLeft)
    {
        switch ((int)timeLeft)
        {
            case 10:
                click.source.pitch = 1.1f;
                break;
            case 9:
                click.source.pitch = 1.2f;
                break;
            case 8:
                click.source.pitch = 1.4f;
                break;
            case 7:
                click.source.pitch = 1.6f;
                break;
            case 6:
                click.source.pitch = 1.8f;
                break;
            case 5:
                click.source.pitch = 2f;
                break;
            case 4:
                click.source.pitch = 2.2f;
                break;
            case 3:
                click.source.pitch = 2.4f;
                break;
            case 2:
                click.source.pitch = 2.6f;
                break;
            case 1:
                click.source.pitch = 2.8f;
                break;
            case 0:
                click.source.pitch = 3f;
                break;
            default:
                click.source.pitch = 1;
                break;
        }
        click.source.Play();
    }

    public void ChangeDesiredPitch(float pitch)
    {
        mainDesiredPitch = pitch;
    }

    private void Update()
    {
        float pitch = main.source.pitch + (mainDesiredPitch - main.source.pitch) * Time.deltaTime * pitchChangeSpeed;
        ChangeMusicPitch(pitch);
    }

    public void ChangeMusicPitch(float pitch)
    {      
        main.source.pitch = pitch;
    }

    public void MuteMainThemeVolume()
    {
        StartCoroutine(Mute());
    }

    public void UnMuteMainThemeVolume()
    {
        StartCoroutine(UnMute());
    }

    IEnumerator UnMute()
    {
        Sound s = Array.Find(sounds, item => item.name == "MainTheme");
        if (s.source.volume != s.volume)
        {
            float volume = s.source.volume;
            while (volume < s.volume)
            {
                volume += 0.01f;
                s.source.volume = volume;
                yield return new WaitForSeconds(0.01f);
            }
            s.source.volume = s.volume;
        }
    }

    IEnumerator Mute()
    {
        Sound s = Array.Find(sounds, item => item.name == "MainTheme");
        float volume = s.source.volume;
        while (volume > 0)
        {
            volume -= 0.01f;
            s.source.volume = volume;
            yield return new WaitForSeconds(0.01f);
        }        
    }
    

    public void PlayChampions()
    {
        MuteMainThemeVolume();
        Play("Champions");
    }

    public void PlayFuneral()
    {
        MuteMainThemeVolume();
        Play("Funeral");
    }

    

    public void Play (string sound)
	{
        Sound s = Array.Find(sounds, item => item.name == sound);
        s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
        s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

        s.source.Play();
    }
}
