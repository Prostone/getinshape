﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraSmoothZoom : MonoBehaviour {
    public GameObject[] cameraBoxes;
    private BoxCollider2D[] rooms;

    public float dampTime = 0.1f;
    public float edgeBufferOrtho = 1f;
    public EdgeBuffer edgeBuffers;
    public bool useSmoothZoom = true;
    private Camera thisCamera;
    public float aspectRatio = 1.778f;
    private Vector3 center;
    private Vector3 velocity = Vector3.zero;
    private float velocityFloat = 0f;

    private void Start()
    {
        thisCamera = GetComponent<Camera>();

        rooms = new BoxCollider2D[cameraBoxes.Length];
        for (int i = 0; i < cameraBoxes.Length; i++)
        {
            rooms[i] = cameraBoxes[i].GetComponent<BoxCollider2D>();
        }
        //aspectRatio = (float)Screen.width / (float)Screen.height;
        SetPositionAndSize();
    }

    public void FixedUpdate()
    {
        SetPositionAndSize();
    }

    public void SetPositionAndSize()
    {
        CameraRoomPos pos = FindCenter();
        if (useSmoothZoom)
        {
            transform.position = Vector3.SmoothDamp(transform.position, pos.center, ref velocity, dampTime);
            thisCamera.orthographicSize = Mathf.SmoothDamp(thisCamera.orthographicSize, pos.orthoSize, ref velocityFloat, dampTime);
        }
        else
        {
            transform.position = pos.center;
            thisCamera.orthographicSize = pos.orthoSize;
        }
        center = pos.center;
    }

    private CameraRoomPos FindCenter()
    {
        CameraRoomPos pos = new CameraRoomPos();
        aspectRatio = (float)Screen.width / (float)Screen.height;
        float maxX = float.MinValue;
        float maxY = float.MinValue;
        float minX = float.MaxValue;
        float minY = float.MaxValue;
        int activeCount = 0;
        for (int i = 0; i < rooms.Length; i++)
        {
            if (rooms[i].gameObject.transform.parent.gameObject.activeSelf)
            {
                maxX = Mathf.Max(rooms[i].bounds.max.x, maxX);
                maxY = Mathf.Max(rooms[i].bounds.max.y, maxY);
                minX = Mathf.Min(rooms[i].bounds.min.x, minX);
                minY = Mathf.Min(rooms[i].bounds.min.y, minY);
                activeCount++;
            }
        }

        if (activeCount == 0)
        {
            pos.center = Vector3.zero;
            pos.orthoSize = 11;
            return pos;
        }

        // Add buffers
        minX -= edgeBuffers.left;
        maxX += edgeBuffers.right;
        minY -= edgeBuffers.bottom;
        maxY += edgeBuffers.top;
        
        pos.center = new Vector3((minX+maxX)/2,(minY + maxY)/2,-10);
        float oy = Mathf.Max(Mathf.Abs(maxY - pos.center.y), Mathf.Abs(minY - pos.center.y));
        float ox = Mathf.Max(Mathf.Abs(maxX - pos.center.x), Mathf.Abs(minX - pos.center.x)) / aspectRatio;
        pos.orthoSize = Mathf.Max(oy, ox) + edgeBufferOrtho;
        return pos;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(center, 2f);
    }
}

[System.Serializable]
public struct EdgeBuffer
{
    public float top;
    public float bottom;
    public float right;
    public float left;
}

public struct CameraRoomPos
{
    public Vector3 center;
    public float orthoSize;
}
