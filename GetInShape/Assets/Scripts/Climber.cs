﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climber : MonoBehaviour {

    public float climbForce = 6f;
    public string buttonPressSoundString;
    public GameObject deadEffectBottom;
    public GameObject deadEffectTop;
    public string destroyerObject = "Hazard";
    public GameObject maskParentEffect;
    Rigidbody2D rb;
    private bool isDead = false;
    public GameObject room;
    private Vector3 startPosition;
    private Quaternion startRotation;

    private void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    void Start () {
        rb = GetComponent<Rigidbody2D>();        
    }

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
        transform.position = startPosition;
        transform.rotation = startRotation;
    }

    // Update is called once per frame
    void Update () {       

		if (Input.GetKeyDown(KeyCode.L))
        {
            Climb();
            AudioManager.instance.Play(buttonPressSoundString);
        }
	}

    public void Climb()
    {
        float force = rb.mass * rb.velocity.x;
        rb.AddForce( Vector2.right * (climbForce - force), ForceMode2D.Impulse);
    }

    public void Die(bool dieBottom)
    {
        Debug.Log("Runner Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(dieBottom ? deadEffectBottom : deadEffectTop, transform.position, Quaternion.identity);
            effect.transform.parent = maskParentEffect.transform;
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }        
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == destroyerObject)
        {
            if (obj.name == "PlayerHazardBottom")
            {
                Die(true);
            }
            else
            {
                Die(false);
            }
        }
    }
}
