﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colorer : MonoBehaviour {

    public GameObject deadEffect;
    public string buttonPressSoundString;
    public ColorWithTag[] colorsTagged;
    private ColorWithTag currentColor;
    private int colorIndex = 0;
    //Rigidbody2D rb;
    SpriteRenderer spriteRenderer;
    private bool isDead = false;
    public GameObject room;

    void Start () {
        //rb = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        ChangeColor(colorIndex);
    }

    private void OnEnable()
    {
        SetStartParameters();       
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
    }

    // Update is called once per frame
    void Update () {        

		if (Input.GetKeyDown(KeyCode.D))
        {
            ChangeToNextColor();
            AudioManager.instance.Play(buttonPressSoundString);
        }
	}
    private void ChangeColor(int colorIndex)
    {
        spriteRenderer.color = colorsTagged[colorIndex].color;
        currentColor = colorsTagged[colorIndex];
    }
    private void ChangeToNextColor()
    {
        colorIndex++;
        if(colorIndex >= colorsTagged.Length)
        {
            colorIndex = 0;
        }
        ChangeColor(colorIndex);
    }

    public void Die()
    {
        Debug.Log("Colorer Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(deadEffect, transform.position, Quaternion.identity);
            effect.transform.parent = this.transform.parent.transform.parent;
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }        
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject obj = collider.gameObject;
        if (obj.tag != currentColor.colorTag)
        {
            Die();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag != currentColor.colorTag)
        {
            Die();
        }
    }
}

[System.Serializable]
public struct ColorWithTag
{
    public Color color;
    public string colorTag;
}