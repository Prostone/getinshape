﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class FadeCanvas : MonoBehaviour {

    public float fadeTime = 5f;
    private CanvasGroup cg;
    public AnimationCurve animCurve;
    public bool activateOnEnable = true;
    private float _timer;
    private bool active = false;
    public bool deactivateObjectAfer = true;

	void Awake ()
    {
        cg = GetComponent<CanvasGroup>();
    }

    private void OnEnable()
    {
        _timer = fadeTime;
        if (activateOnEnable)
            active = true;
    }

    public void Activate()
    {
        active = true;        
    }

    void Update ()
    {
        if (!active) { return; }

        if (_timer >=0)
        {
            cg.alpha = animCurve.Evaluate(_timer / fadeTime);
            _timer -= Time.deltaTime;
        }
        else
        {
            active = false;
            if (deactivateObjectAfer)
                gameObject.SetActive(false);
        }        
	}
}
