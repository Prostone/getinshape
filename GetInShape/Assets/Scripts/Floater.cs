﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floater : MonoBehaviour {

    public float moveSpeed = 6f;
    public string buttonPressSoundString;
    public GameObject deadEffect;
    Rigidbody2D rb;
    private bool isDead = false;
    private Vector2 direction = Vector2.up;
    public GameObject room;

    private Vector3 startPosition;
    private Quaternion startRotation;

    private void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
        transform.position = startPosition;
        transform.rotation = startRotation;
    }

    // Update is called once per frame
    void Update () {        

		if (Input.GetKeyDown(KeyCode.S))
        {
            direction *= -1;
            AudioManager.instance.Play(buttonPressSoundString);
        }
	}

    private void FixedUpdate()
    {
        Move(direction);
    }

    public void Move(Vector2 direction)
    {
        rb.MovePosition(rb.position + direction * moveSpeed * Time.fixedDeltaTime);
    }

    public void Die()
    {
        Debug.Log("Floater Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(deadEffect, transform.position, Quaternion.identity);
            effect.transform.parent = this.transform.parent;
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }        
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == "Destroyable")
        {
            Die();
        }
    }
}
