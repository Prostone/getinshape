﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour {

    public bool debugMode = false;
    public bool changePitchActive = true;
    public GameObject[] rooms;
    public Text timer;
    public Text completeTimer;
    public GameObject overPanel;
    public GameObject completePanel;
    public GameObject pauseMenu;
    public GameObject difficultyPanel;
    public GameObject livesPanel;
    public Slider livesSlider;
    public Text survivedText;
    public Text highScoreText;
    public Text completeTimeText;
    public Text bestScoreText;
    public Text surviveText;
    public float secondActiveTime = 10;
    public float otherActiveTime = 20;
    public float thresholdForTimerDecrease = 5f;
    public float decreaseActivateValue = 3f;
    [HideInInspector] //Set by difficulty
    public float completeTime = 20f;
    private float currentSpawnTimer = 0f;
    private float highScore;
    private float completeBestTime;

    private List<GameObject> inactiveRooms = new List<GameObject>();
    private float currentTime;
    private float currentCompleteTime;
    private bool gameIsOver = false;
    private bool gameCompleted = false;
    private bool gameRunning = false;
    private float lastCompleteSound = 0f;

    // Difficulty
    private int _maxLives;
    public int maxLives { get { return _maxLives; } set { _maxLives = value; currentLives = value; } }
    private int currentLives = 20;
    [HideInInspector] //Set by difficulty
    public int maxRoomsActive = 6;
    public float startGameInvoke = 4f;

    [HideInInspector]
    public static GameMaster instance;

    private void Start()
    {
        Time.timeScale = 1f;
        if (instance == null)
        {
            instance = this;
        }

        if (debugMode)
        {
            StartGame();
        }
             
    }    

    public void StartGame()
    {
        gameRunning = true;

        highScore = PlayerPrefs.GetFloat("HighScore", 0f);
        completeBestTime = PlayerPrefs.GetFloat("BestTime", float.MaxValue);

        currentLives = maxLives;

        currentTime = 0f;
        currentSpawnTimer = secondActiveTime;
        for (int i = 0; i < rooms.Length; i++)
        {
            AddRoomToInactive(rooms[i], false);
        }
        // SPAWN FIRST ROOM
        if (!debugMode)
            SetRandomRoomActive();
        if (!debugMode) { ChangePitchBasedOnRooms(); }       
        Invoke("Unmute", 0.2f); // Just cuz there were some initialization problems, meh.
        gameIsOver = false;
        gameCompleted = false;
        //gameIsOver = true;

        currentCompleteTime = completeTime;
        lastCompleteSound = currentCompleteTime;
    }

    private void Update()
    {
        if (debugMode) { return; }
        if (!gameRunning) { return; }
        if (gameIsOver) { return; };

        ChangePitchBasedOnRooms();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }


        if (!gameIsOver)
        {
            currentTime += Time.deltaTime;
            ShowTime();
        }

        if (inactiveRooms.Count > 0 && inactiveRooms.Count > 6 - maxRoomsActive)
        {
            currentSpawnTimer -= Time.deltaTime;
            if (currentSpawnTimer <= 0)
            {
            
                    SetRandomRoomActive();
                    currentSpawnTimer = otherActiveTime;            
            }
        }

        if ((inactiveRooms.Count == rooms.Length || currentLives <=  0) && !gameIsOver)
        {            
            GameOver();
        }

        if (gameCompleted == false)
        { 
            if (inactiveRooms.Count == 0 || inactiveRooms.Count == 6 - maxRoomsActive)
            {
                currentCompleteTime -= Time.deltaTime;
                currentCompleteTime = Mathf.Clamp(currentCompleteTime, 0f, float.MaxValue);
                ShowCompleteTime();

                if (lastCompleteSound - currentCompleteTime >= 1f)
                {
                    AudioManager.instance.PlayTimerClick(currentCompleteTime);
                    lastCompleteSound = currentCompleteTime;
                }

                if (!completeTimer.transform.parent.gameObject.activeSelf) { completeTimer.transform.parent.gameObject.SetActive(true); }

                if (currentCompleteTime <= 0f)
                {
                    gameCompleted = true;
                    GameComplete();
                }
            }
            else
            {
                lastCompleteSound = currentCompleteTime;
                currentCompleteTime = completeTime;
                if (completeTimer.transform.parent.gameObject.activeSelf) { completeTimer.transform.parent.gameObject.SetActive(false); }
            }
        }
    }

    void Unmute()
    {
        if (AudioManager.instance != null)
            AudioManager.instance.UnMuteMainThemeVolume();
    }

    public void TogglePause()
    {
        if (gameIsOver)
        {
            return;
        }

        AudioManager.instance.Play("Click");
        pauseMenu.SetActive(!pauseMenu.activeSelf);

        if (pauseMenu.activeSelf)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    private void DecreaseTimerForNewRoom()
    {
        if (currentSpawnTimer > thresholdForTimerDecrease)
        {
            currentSpawnTimer = decreaseActivateValue;
        }
        
    }

    private void ChangePitchBasedOnRooms()
    {
        if (AudioManager.instance != null && changePitchActive)
            AudioManager.instance.ChangeDesiredPitch((float)(rooms.Length - inactiveRooms.Count - 1) / (rooms.Length - 1) * 0.6f + 1);
    }

    private void GameComplete()
    {
        gameIsOver = true;
        highScore = Mathf.Max(currentTime, highScore);
        PlayerPrefs.SetFloat("HighScore", highScore);

        completeBestTime = Mathf.Min(currentTime, completeBestTime);
        Debug.Log(completeBestTime);
        Debug.Log(currentTime);
        PlayerPrefs.SetFloat("BestTime", completeBestTime);

        completePanel.SetActive(true);

        completeTimeText.text = currentTime.ToString("#0.00");
        bestScoreText.text = "BEST TIME: " + completeBestTime.ToString("#0.00") + " sec";
        AudioManager.instance.PlayChampions();

        livesPanel.SetActive(false);
        timer.text = "";
    }

    private void GameOver()
    {
        gameIsOver = true;
        highScore = Mathf.Max(currentTime, highScore);
        PlayerPrefs.SetFloat("HighScore", highScore);
        overPanel.SetActive(true);
        survivedText.text = currentTime.ToString("#0.00");
        highScoreText.text = "HIGHSCORE: " + highScore.ToString("#0.00") + " sec";
        AudioManager.instance.PlayFuneral();

        livesPanel.SetActive(false);
        timer.text = "";
    }

    public void AddRoomToInactive(GameObject room, bool decreaseTimer = true)
    {
        inactiveRooms.Add(room);
        
        if (decreaseTimer)
        {
            DecreaseTimerForNewRoom();
            currentLives--;
            SetSliderValue(currentLives);
        }            
    }

    private void ShowCompleteTime()
    {
        completeTimer.text = currentCompleteTime.ToString("#0.00");
    }

    private void ShowTime()
    {
        timer.text = currentTime.ToString("#0.00");
    }

    private void SetRandomRoomActive()
    {
        int rngsus = Random.Range(0, inactiveRooms.Count);
        inactiveRooms[rngsus].SetActive(true);
        inactiveRooms.RemoveAt(rngsus);
    }

    private void SetLivesSliderValues()
    {
        livesSlider.maxValue = maxLives;
        livesSlider.value = maxLives;
    }

    private void SetSliderValue(float value)
    {
        if (value >= 0 && value <= livesSlider.maxValue)
            livesSlider.value = value;
    }

    private void SetSurviveText(float sec,int rooms)
    {
        string text = "";
        text += "SURVIVE FOR <color=yellow><b>";
        text += ((int)sec).ToString() + " SECONDS </b></color> WITH <color=yellow><b>";
        text += rooms.ToString() + " ROOMS </b></color> ACTIVE TO WIN";

        surviveText.text = text;
        surviveText.supportRichText = true;
    }

    public enum Difficulty
    {
        Easy,
        Medium,
        Hard
    }

    public void DifficultyIntSet(int diff)
    {
        SetDifficulty((Difficulty)diff);
    }

    public void SetDifficulty(Difficulty diff)
    {
        switch (diff)
        {
            case Difficulty.Easy:
                maxLives = 16;
                maxRoomsActive = 4;
                completeTime = 30f;
                break;
            case Difficulty.Medium:
                maxLives = 12;
                maxRoomsActive = 5;
                completeTime = 25f;
                break;
            case Difficulty.Hard:
                maxLives = 20;
                maxRoomsActive = 6;
                completeTime = 20f;
                break;
            default:
                maxLives = 8;
                maxRoomsActive = 6;
                completeTime = 20f;
                break;
        }
        SetLivesSliderValues();
        SetSurviveText(completeTime, maxRoomsActive);
        surviveText.gameObject.SetActive(true);
        difficultyPanel.GetComponent<FadeCanvas>().Activate();
        livesPanel.SetActive(true);
        Invoke("StartGame", startGameInvoke);
    }

    public void RestartGame()
    {
        AudioManager.instance.Play("Click");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitGame()
    {
        AudioManager.instance.Play("Click");
        Application.Quit();
    }
}
