﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideBasedOnPlatform : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            gameObject.SetActive(false);
        }

        //if (Application.platform == RuntimePlatform.WindowsEditor)
        //{
        //    gameObject.SetActive(false);
        //}
    }
	
}
