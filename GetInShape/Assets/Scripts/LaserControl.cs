﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserControl : MonoBehaviour {

    public float laserForce = 6f;
    public string buttonPressSoundString;
    public string destroyTag = "Hazard";
    public LaserGun[] laserGuns;
    private int currentlyActiveGun = 0;
    public GameObject room;

    public GameObject deadEffect;
    Rigidbody2D rb;
    [HideInInspector]
    public bool isDead = false;

    private Vector3 startPosition;
    private Quaternion startRotation;

    private void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //laserGuns[0].SetTarget(gameObject);
        //laserGuns[1].SetTarget(gameObject);               
    }

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
        transform.position = startPosition;
        transform.rotation = startRotation;
        Invoke("StartFiring", 1f);
    }

    void StartFiring()
    {
        laserGuns[currentlyActiveGun].PrepareToFire();
    }

    // Update is called once per frame
    void Update()
    {
        //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, raycastDistance, layerMask);
        //Debug.DrawRay(transform.position, Vector3.down * raycastDistance, Color.red);


        if (Input.GetKeyDown(KeyCode.J))
        {
            laserGuns[currentlyActiveGun].StopDoingStuff();
            currentlyActiveGun = Mathf.Abs(currentlyActiveGun - 1);
            laserGuns[currentlyActiveGun].PrepareToFire();
            AudioManager.instance.Play(buttonPressSoundString);
        }
    }

    public void Move(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + direction.normalized * laserForce * Time.fixedDeltaTime);
    }


    public void Die()
    {
        Debug.Log("Lasered Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(deadEffect, transform.position, Quaternion.identity);
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == destroyTag)
        {
            Die();
        }
    }
}


