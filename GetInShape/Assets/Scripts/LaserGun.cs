﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserGun : MonoBehaviour
{
    public GameObject laserHolder;
    public Transform laserPointer;
    public LineRenderer lineRenderer;
    public ParticleSystem impactEffect;
    public bool isFiring = false;
    public bool isReady = false;
    private bool isMovingBack = false;
    private bool isMovingToPosition = false;
    private Vector2 startPosition;
    public Vector2 firePositionRelative;
    public float moveSpeed = 5f;
    private float t = 0;
    public GameObject target;
    private LaserControl tLaserControl;

    private void Awake()
    {
        startPosition = transform.position;
        StopImpactEffect();
    }

    private void Start()
    {        
        lineRenderer = GetComponent<LineRenderer>();
        tLaserControl = target.GetComponent<LaserControl>();        
    }

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        transform.position = startPosition;
        isFiring = false;
        isReady = false;
        isMovingBack = false;
        isMovingToPosition = false;
        lineRenderer.enabled = false;
        StopImpactEffect();
    }

    public void Fire()
    {
        tLaserControl.Move(target.transform.position - transform.position);

        if (!lineRenderer.enabled)
        {
            lineRenderer.enabled = true;            
        }

        lineRenderer.SetPosition(0, laserPointer.transform.position);
        lineRenderer.SetPosition(1, target.transform.position);

        Vector3 dir = laserPointer.position - target.transform.position;

        impactEffect.transform.position = target.transform.position + dir.normalized * 0.5f; // 1f radius of enemy
        impactEffect.transform.rotation = Quaternion.LookRotation(dir);
    }

    public void PrepareToFire()
    {
        isMovingToPosition = true;
        t = Mathf.InverseLerp(startPosition.x, startPosition.x + firePositionRelative.x, laserHolder.transform.position.x);
        //t = 0;
    }

    public void StopDoingStuff()
    {
        isFiring = false;
        lineRenderer.enabled = false;
        isMovingBack = true;
        t = Mathf.InverseLerp(startPosition.x + firePositionRelative.x, startPosition.x, laserHolder.transform.position.x);
        isMovingToPosition = false;
        //t = 0;
        StopImpactEffect();
    }


    // PARTICLESYSTEM.PLAY() and .STOP() IS BUGGED FOR SOME REASON, SO DIRTY HACKY WORKAROUND HERE I COME!
    public void StopImpactEffect()
    {
        impactEffect.transform.position = Vector2.one * 999f;
    }


    private void Update()
    {
        if (isMovingBack)
        {            
            t += moveSpeed * Time.deltaTime;
            t = Mathf.Clamp01(t);
            //laserHolder.transform.position = Vector2.Lerp(laserHolder.transform.position, startPosition, t);
            laserHolder.transform.position = Vector2.Lerp(startPosition + firePositionRelative, startPosition, t);
            if (t == 1)
            {
                isReady = true;
                isMovingBack = false;
            }
        }
        if (isMovingToPosition)
        {            
            t += moveSpeed * Time.deltaTime;
            t = Mathf.Clamp01(t);
            //laserHolder.transform.position = Vector2.Lerp(laserHolder.transform.position, startPosition + firePositionRelative, t);
            laserHolder.transform.position = Vector2.Lerp(startPosition, startPosition + firePositionRelative, t);
            if (t == 1)
            {
                isReady = true;
                isFiring = true;
                isMovingToPosition = false;
            }
        }
        if (isFiring)
        {
            if(!tLaserControl.isDead)
            {
                 Fire();
            }
            else
            {
                StopDoingStuff();
            }
        }
    }
}