﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    private bool isMoving = false;
    private float moveSpeed;
    private Vector2 direction;

    private void Update()
    {
        if (isMoving)
        {
            transform.Translate(direction * moveSpeed * Time.deltaTime);
        }
    }

    public void StartMoving(Vector2 dir, float speed)
    {
        isMoving = true;
        direction = dir;
        moveSpeed = speed;
    }

    private void OnDisable()
    {
        Destroy(gameObject);
    }
}
