﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDestroyer : MonoBehaviour {

    public List<string> destroyableTags;

    private void OnTriggerEnter2D(Collider2D collision)
    {        
        if (destroyableTags.Contains(collision.gameObject.tag))
        {
            Destroy(collision.transform.parent.gameObject);
        }
    }
}
