﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaleSpawner : MonoBehaviour {
    public float obstacleSpeed = 5f;
    public float startDelay = 3f;
    public float minSpawnTime = 2f;
    public float maxSpawnTime = 5f;
    public bool positionRandomized = true;
    public GameObject[] startPositions;
    public GameObject spriteMaskParent;
    public GameObject[] obstacles;

    private void OnEnable()
    {
        StartCoroutine(SpawnObstacle());
    }

    IEnumerator SpawnObstacle()
    {
        yield return new WaitForSeconds(startDelay);
        while(true)
        {
            int obstacleIndex = Random.Range(0, obstacles.Length);
            Vector2 startPos = transform.position;
            if (positionRandomized)
            {
                int index = Random.Range(0, startPositions.Length);
                startPos = startPositions[index].transform.position;
            }
            GameObject obstacle = Instantiate(obstacles[obstacleIndex], startPos, Quaternion.identity);
            obstacle.transform.SetParent(spriteMaskParent.transform, true);
            obstacle.GetComponent<Obstacle>().StartMoving(transform.TransformVector(Vector2.up), obstacleSpeed);
            float waitBeforeNextWave = Random.Range(minSpawnTime, maxSpawnTime);
            yield return new WaitForSeconds(waitBeforeNextWave);
        }        
   }

}

