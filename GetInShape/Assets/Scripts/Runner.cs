﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Runner : MonoBehaviour {

    public float jumpForce = 6f;
    public string buttonPressSoundString;
    public float raycastDistance = 0.35f;
    public LayerMask layerMask;
    public GameObject deadEffect;
    public GameObject room;
    Rigidbody2D rb;
    private bool isGrounded = false;
    private bool isDead = false;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
    }

    // Update is called once per frame
    void Update () {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, raycastDistance, layerMask);
        Debug.DrawRay(transform.position, Vector3.down * raycastDistance, Color.red);

        if (hit.collider != null)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }

		if (Input.GetKeyDown(KeyCode.A))
        {
            Jump();
        }
	}

    public void Jump()
    {
        if (isGrounded)
        {
            AudioManager.instance.Play(buttonPressSoundString);
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    public void Die()
    {
        Debug.Log("Runner Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(deadEffect, transform.position, Quaternion.identity);
            effect.transform.parent = this.transform.parent;
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }        
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == "Destroyable")
        {
            Die();
        }
    }
}
