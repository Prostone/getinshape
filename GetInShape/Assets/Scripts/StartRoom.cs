﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartRoom : MonoBehaviour {

    public GameObject player;
    public GameObject obstacleParent;
    public string enableRoomSoundString;
    public string destroyTag = "Destroyable";

    private void OnEnable()
    {
        if (!player.activeSelf)
            player.SetActive(true);

        AudioManager.instance.Play(enableRoomSoundString);
        //if (obstacleParent != null)
        //{
        //    for (int i = 0; i < obstacleParent.transform.childCount; i++)
        //    {
        //        if (obstacleParent.transform.GetChild(i))
        //    }
        //}
    }
}
