﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swimmer : MonoBehaviour {

    public float diveForce = 6f;
    public string buttonPressSoundString;
    public GameObject deadEffect;
    public GameObject waterRiser;
    public ParticleSystem dripEffectLeft;
    public ParticleSystem dripEffectRight;
    public float risingSpeed = 2f;
    private float currentRisingSpeed;
    public AnimationCurve risingCurve;
    public AnimationCurve drainingCurve;
    public float drainingSpeed = 4f;
    private float currentDrainSpeed = 0f;
    public float waterRiserMaxScale = 6f;
    private BuoyancyEffector2D waterEffector;
    public Vector2 flowChangeRangeSeconds;
    public float flowChangeLocalPosition = 4.5f;
    public string drainObjectTag = "Drain";
    public GameObject oxygenShower;
    public float oxygenStart = 100f;
    private float currentOxygen;
    public float oxygenDecrease = 1f;
    Rigidbody2D rb;
    private bool isDead = false;
    private bool continueRising = true;
    private bool isDraining = false;
    private bool isDiving = false;
    private bool openToFlowChange = true;
    private float lastFlowXPos;
    public GameObject room;

    private float minScale = 0.5f;
    private float maxScale = 7f;

    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startWaterScale;

    private void Awake()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startWaterScale = waterRiser.transform.localScale;
    }

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        waterEffector = waterRiser.GetComponent<BuoyancyEffector2D>();
        currentOxygen = oxygenStart;
        ChangeDripEffect();
        Invoke("ChangeFlow", 0.5f);
    }

    private void OnEnable()
    {
        SetStartParameters();
    }

    public void SetStartParameters()
    {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        isDead = false;
        transform.position = startPosition;
        transform.rotation = startRotation;
        currentOxygen = oxygenStart;
        lastFlowXPos = transform.localPosition.x;
        openToFlowChange = true;
        waterRiser.transform.localScale = startWaterScale;
    }

    private void ChangeFlow()
    {        
        if (!openToFlowChange)
        {
            if(Mathf.Sign(lastFlowXPos) != Mathf.Sign(transform.localPosition.x))
            {
                openToFlowChange = true;
            }
        }
        if (openToFlowChange && (transform.localPosition.x > flowChangeLocalPosition
                              || transform.localPosition.x < -flowChangeLocalPosition))
        {
            waterEffector.flowAngle = Mathf.Abs(waterEffector.flowAngle - 180);
            lastFlowXPos = transform.localPosition.x;
            openToFlowChange = false;
            ChangeDripEffect();
        }
        Invoke("ChangeFlow", Random.Range(flowChangeRangeSeconds.x, flowChangeRangeSeconds.y));
    }

    void ChangeDripEffect()
    {
        if (waterEffector.flowAngle < 90)
        {
            dripEffectRight.Stop();
            dripEffectLeft.Play();
        }
        else
        {
            dripEffectRight.Play();
            dripEffectLeft.Stop();
        }
    }


    private void SetOxygen()
    {
        if (waterEffector.GetComponent<BoxCollider2D>().bounds.max.y > transform.GetComponent<BoxCollider2D>().bounds.max.y)
        {
            currentOxygen -= oxygenDecrease * Time.deltaTime;
        }
        else
        {
            currentOxygen += oxygenDecrease * 2 *Time.deltaTime;
        }
        currentOxygen = Mathf.Clamp(currentOxygen, 0, oxygenStart);
        Vector3 newScale = oxygenShower.transform.localScale;
        newScale.x = currentOxygen / oxygenStart;
        oxygenShower.transform.localScale = newScale;

        if (currentOxygen == 0)
        {
            Die();
        }
    }

    // Update is called once per frame
    void Update () {

        SetOxygen();
        //if (waterRiser.transform.localScale.y >= waterRiserMaxScale)
        //{
        //continueRising = false;
        //}

        if (continueRising)
        {            
            Vector3 newScale = waterRiser.transform.localScale;
            currentRisingSpeed = risingCurve.Evaluate(newScale.y / maxScale) * risingSpeed;
            if (isDraining)
            {
                //Debug.Log(drainingCurve.Evaluate(newScale.y / maxScale));
                currentDrainSpeed = drainingCurve.Evaluate(newScale.y / maxScale) * drainingSpeed;
            }
            else
            {
                currentDrainSpeed = 0f;
            }
            float newScaleY = newScale.y + (-currentDrainSpeed + currentRisingSpeed) * Time.deltaTime;
            newScaleY = Mathf.Clamp(newScaleY, minScale, maxScale);
            waterRiser.transform.localScale = new Vector3(newScale.x , newScaleY, newScale.z);
            currentDrainSpeed = 0;
        }

        if (Input.GetKey(KeyCode.K))
        {
            isDiving = true;
            AudioManager.instance.Play(buttonPressSoundString);
        }
        else
        {
            isDiving = false;
        }
	}

    private void FixedUpdate()
    {
        if(isDiving)
            Dive();
    }

    public void Dive()
    {       
        //rb.AddForce(Vector2.down * diveForce, ForceMode2D.Force);
        rb.MovePosition((Vector2)transform.position + Vector2.down * diveForce * Time.fixedDeltaTime);
    }

    public void Die()
    {
        Debug.Log("Swimmer Died");
        if (!isDead)
        {
            gameObject.SetActive(false);
            GameObject effect = Instantiate(deadEffect, transform.position, Quaternion.identity);            
            isDead = true;
            Destroy(effect, 3f);
            Invoke("EndRoom", 2f);
        }        
    }

    public void EndRoom()
    {
        room.SetActive(false);
        GameMaster.instance.AddRoomToInactive(room);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == drainObjectTag)
        {
            isDraining = true;
            SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
            Color c = sr.color;
            c.a = 0.5f;
            sr.color = c;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;
        if (obj.tag == drainObjectTag)
        {
            isDraining = false;
            SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
            Color c = sr.color;
            c.a = 1f;
            sr.color = c;
        }
    }
}
